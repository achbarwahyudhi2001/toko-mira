-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Des 2022 pada 09.43
-- Versi server: 10.4.25-MariaDB
-- Versi PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_online`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_barang`
--

CREATE TABLE `tb_barang` (
  `Id_brg` int(11) NOT NULL,
  `Nama_brg` varchar(120) NOT NULL,
  `Keterangan` varchar(225) NOT NULL,
  `Kategori` varchar(60) NOT NULL,
  `Harga` int(11) NOT NULL,
  `Stok` int(4) NOT NULL,
  `Gambar` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_barang`
--

INSERT INTO `tb_barang` (`Id_brg`, `Nama_brg`, `Keterangan`, `Kategori`, `Harga`, `Stok`, `Gambar`) VALUES
(14, 'Kemeja', 'kemeja lengan panjang | warna red gray | bahan flannel', 'Kemeja', 95000, 21, 'kemeja3.jpg'),
(15, 'kemeja', 'kemeja lengan panjang | warna hitam gray | bahan soft flannel', 'Kemeja', 95000, 33, 'kemeja4.jpg'),
(16, 'kemeja', 'kemeja lengan panjang| warna abu-abu gray | bahan soft flannel', 'Kemeja', 95000, 23, 'kemeja5.jpg'),
(17, 'kemeja', 'kemeja lengan pendek | warna abu-abu | bahan katun', 'Kemeja', 90000, 55, 'kemeja6.jpg'),
(18, 'kemeja', 'kemeja lengan pendek | warna abu terang | bahan katun', 'Kemeja', 90000, 56, 'kemeja6.jpg'),
(19, 'kemeja', 'kemeja lengan pendek | warna latte| bahan katun', 'Kemeja', 90000, 56, 'kemeja9.jpg'),
(20, 'celana', 'celana xpd chinos | warna cream | bahan katun', 'Celana', 110000, 43, 'jeans2.jpg'),
(21, 'celana', 'celana xpd chinos | warna cream terang | bahan katun', 'Celana', 110000, 43, 'jeans3.jpg'),
(22, 'celana', 'celana xpd chinos | warna abu terang| bahan katun', 'Celana', 110000, 43, 'jeans4.jpg'),
(23, 'celana', 'celana xpd chinos | warna abu tua| bahan katun', 'Celana', 110000, 43, 'jeans5.jpg'),
(24, 'celana', 'celana panjang kargo pinggang karet | warna cream | bahan katun halus', 'Celana', 98000, 27, 'joggerpanjang2.jpg'),
(25, 'celana', 'celana panjang kargo pinggang karet | warna moka | bahan katun halus', 'Celana', 98000, 27, 'joggerpanjang3.jpg'),
(26, 'celana', 'celana panjang kargo pinggang karet | warna latte | bahan katun halus', 'Celana', 98000, 26, 'joggerpanjang4.jpg'),
(27, 'celana', 'celana panjang kargo pinggang karet | warna hitam | bahan katun halus', 'Celana', 98000, 26, 'joggerpanjang5.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_invoice`
--

CREATE TABLE `tb_invoice` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `tgl_pesan` datetime NOT NULL,
  `batas_bayar` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_invoice`
--

INSERT INTO `tb_invoice` (`id`, `nama`, `alamat`, `tgl_pesan`, `batas_bayar`) VALUES
(2, '', '', '2022-12-03 19:37:22', '2022-12-04 19:37:22'),
(16, 'rixal', 'ngaliayn', '2022-12-12 11:20:05', '2022-12-13 11:20:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pesanan`
--

CREATE TABLE `tb_pesanan` (
  `id` int(11) NOT NULL,
  `id_invoice` int(11) NOT NULL,
  `id_brg` int(11) NOT NULL,
  `nama_brg` varchar(50) NOT NULL,
  `jumlah` int(3) NOT NULL,
  `harga` int(10) NOT NULL,
  `pilihan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pesanan`
--

INSERT INTO `tb_pesanan` (`id`, `id_invoice`, `id_brg`, `nama_brg`, `jumlah`, `harga`, `pilihan`) VALUES
(1, 2, 1, 'Sepatu', 1, 300000, ''),
(2, 2, 2, 'Kamera', 1, 5000000, ''),
(3, 3, 2, 'Kamera', 1, 5000000, ''),
(4, 3, 3, 'Handphone', 1, 3400000, ''),
(5, 3, 7, 'Laptop', 1, 4500000, ''),
(6, 4, 2, 'Kamera', 1, 5000000, ''),
(7, 5, 1, 'Sepatu', 1, 300000, ''),
(8, 1, 2, 'Jam Tangan', 1, 550000, ''),
(9, 1, 8, 'Sepatu Vans', 1, 500000, ''),
(10, 1, 5, 'Celana Jogger', 1, 200000, ''),
(11, 1, 6, 'Kaos Polos', 1, 65000, ''),
(12, 2, 4, 'Topi Baseball', 1, 55000, ''),
(13, 3, 6, 'Kaos Polos', 1, 65000, ''),
(14, 3, 5, 'Celana Jogger', 1, 200000, ''),
(15, 4, 27, 'celana', 1, 98000, ''),
(16, 4, 26, 'celana', 1, 98000, ''),
(17, 16, 16, 'kemeja', 1, 95000, ''),
(18, 16, 17, 'kemeja', 1, 90000, '');

--
-- Trigger `tb_pesanan`
--
DELIMITER $$
CREATE TRIGGER `pesanan_penjualan` AFTER INSERT ON `tb_pesanan` FOR EACH ROW BEGIN
	UPDATE tb_barang SET Stok = Stok-NEW.jumlah
    WHERE Id_brg = NEW.Id_brg;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role_id` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `nama`, `username`, `password`, `role_id`) VALUES
(1, 'admin', 'admin', '123', 1),
(2, 'user', 'user', '123', 2),
(5, 'achbar', 'achbar', '123', 2),
(6, 'ach', 'ach', '123', 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`Id_brg`);

--
-- Indeks untuk tabel `tb_invoice`
--
ALTER TABLE `tb_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_pesanan`
--
ALTER TABLE `tb_pesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_barang`
--
ALTER TABLE `tb_barang`
  MODIFY `Id_brg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `tb_invoice`
--
ALTER TABLE `tb_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tb_pesanan`
--
ALTER TABLE `tb_pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
