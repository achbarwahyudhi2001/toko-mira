<?php 
class Kategori extends CI_Controller{
	public function kaos(){
		$data['kaos'] = $this->model_kategori->data_kaos()->result();
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('kaos', $data);
		$this->load->view('templates/footer');
	}

	public function kemeja(){
		$data['kemeja'] = $this->model_kategori->data_kemeja()->result();
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('kemeja', $data);
		$this->load->view('templates/footer');
	}

	public function celana(){
		$data['celana'] = $this->model_kategori->data_celana()->result();
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('celana', $data);
		$this->load->view('templates/footer');
	}

}
