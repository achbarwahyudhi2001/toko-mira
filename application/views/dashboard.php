<div class="container-fluid">

	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class="d-block w-100" src="<?php echo base_url('assets/img/sale3.jpg') ?>" alt="First slide">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="<?php echo base_url('assets/img/sale2.jpg') ?>" alt="Second slide">
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	<div class="row text-center mt-4">

		<?php foreach ($barang as $brg) : ?>
			<div class="card ml-3 mb-3" style="width: 16rem;">
				<img src="<?php echo base_url() . './uploads/' . $brg->Gambar ?>" width="640" height="225" overflow="hidden" class="card-img-top" alt="...">
				<div class="card-body">
					<h5 class="card-title mb-1 text-dark font-weight-bold"><?php echo $brg->Nama_brg ?></h5>
					<small><?php echo $brg->Keterangan ?></small><br>
					<span class="badge badge-warning my-2 text-white">Rp. <?php echo number_format($brg->Harga), 0, ',', '.' ?></span>
					<?php echo anchor('dashboard/tambah_ke_keranjang/' . $brg->Id_brg, '<div class="btn btn-sm btn-primary">Tambah ke Keranjang</div>')  ?>
					<?php echo anchor('dashboard/detail/' . $brg->Id_brg, '<div class="btn btn-sm btn-secondary">Detail</div>')  ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>

<footer class="bg-dark text-white mt-3">
	<div class="row">
		<div class="col-md-12 text-center font-weight-light">
			<p class="mt-3">Developed Achbar Wahyudhi Copyright <i class="far fa-copyright"></i> 2022</p>
		</div>
	</div>
</footer>
<!--  
    <div class="copyright text-white text-center font-weight-bold bg-warning p-1">
      <p class="mt-3">Developed Achbar Wahyudhi Copyright<i class="far fa-copyright"></i>2022</p>
    </div> -->